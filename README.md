# ansible-role-users

Role to manage users and groups on a system.

## Role configuration

* users_create_per_user_group (default: true) - when creating users, also
  create a group with the same username and make that the user's primary
  group.
* users_group (default: users) - if users_create_per_user_group is _not_ set,
  then this is the primary group for all created users.
* users_default_shell (default: /bin/bash) - the default shell if none is
  specified for the user.
* users_create_homedirs (default: true) - create home directories for new
  users. Set this to false if you manage home directories separately.
* authorized_keys_file (default: .ssh/authorized_keys) - Set this if the
  ssh server is configured to use a non standard authorized keys file.

## Creating users

Add a users variable containing the list of users to add. A good place to put
this is in `group_vars/all` or `group_vars/groupname` if you only want the
users to be on certain machines.

The following attributes are required for each user:

* username - The user's username.
* name - The full name of the user (gecos field).
* home - The home directory of the user to create (optional, defaults to /home/username).
* uid - The numeric user id for the user (optional). This is required for uid consistency
  across systems.
* gid - The numeric group id for the group (optional). Otherwise, the
  uid will be used.
* password - If a hash is provided then that will be used, but otherwise the
  account will be locked.
* update_password - This can be either 'always' or 'on_create'
  - 'always' will update passwords if they differ. (default)
  - 'on_create' will only set the password for newly created users.
* group - Optional primary group override.
* groups - A list of supplementary groups for the user.
* append - If yes, will only add groups, not set them to just the list in groups (optional).
* profile - A string block for setting custom shell profiles.
* ssh_key - This should be a list of SSH keys for the user (optional). Each SSH key
  should be included directly and should have no newlines.
* generate_ssh_key - Whether to generate a SSH key for the user (optional, defaults to no).

In addition, the following items are optional for each user:

* shell - The user's shell. This defaults to /bin/bash. The default is
  configurable using the users_default_shell variable if you want to give all
  users the same shell, but it is different than /bin/bash.

Example:

```yml
---
users_create_per_user_group: false

users_group: users

users_default_shell: /bin/ksh

users_create_homedirs: false

users:
  - username: amddba
    name: Admin and monitoring User
    home: /opt/SP/dba/amddba
    uid: 66711
    group: 30386
    groups: 30014
  - username: aim_dba
    name: Admin and monitoring User
    home: /opt/SP/dba/aim_dba
    uid: 16452
    group: 30386
    groups: 30014

groups_to_create:
  - name: aimsys
    gid: 16700
  - name: tact
    gid: 1846
  - name: prlog
    gid: 16006
```